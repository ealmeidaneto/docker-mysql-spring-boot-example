FROM maven:3.3.9 AS build  
COPY src /usr/src/app/src  
COPY pom.xml /usr/src/app  
RUN mvn -f /usr/src/app/pom.xml clean compile package

FROM openjdk:8
WORKDIR /usr/app/
COPY --from=build /usr/src/app/target/users-mysql.jar /usr/app/users-mysql.jar
#ADD target/users-mysql.jar /usr/app/users-mysql.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/usr/app/users-mysql.jar"]